<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sentence extends Model
{
    public $incrementing = false;

    use SoftDeletes;

    public function getKeyType()
    {
        return 'string';
    }

    public function call () {
        return $this->belongsTo(Call::class);
    }

    public function user () {
        return $this->belongsTo(User::class);
    }
}
