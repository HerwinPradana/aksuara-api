<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Call extends Model
{
    public $incrementing = false;
    public $timestamps = false;

    use SoftDeletes;

    public function getKeyType()
    {
        return 'string';
    }

    public function host () {
        return $this->hasOne(User::class);
    }

    public function sentences () {
        return $this->hasMany(Sentence::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'call_users', 'call_id', 'user_id')->withPivot('join_time', 'leave_time');
    }
}
