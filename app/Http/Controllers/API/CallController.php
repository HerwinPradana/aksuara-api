<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use App\Models\Call;
use App\Models\Sentence;
use App\Models\User;

class CallController extends Controller {

    public function map (Request $request) {
        $items = Sentence::with('call')->get();
        return response()->json(['count' => $items->count(), 'items' => $items], 200);
    }

    public function start (Request $request) {
        $user = User::first(); // TODO: Replace with $request->user later

        $code = rand(000000, 999999);
        $duplicate = Call::where('code', $code)->first();
        while (!empty($duplicate)) {
            $code = rand(000000, 999999);
            $duplicate = Call::where('code', $code)->first();
        }

        $item = new Call();
        $item->id = (string) Str::orderedUuid();
        $item->host_id = $user->id;
        $item->code = $code;
        $item->started_at = (string) Carbon::now();
        $item->started_by = $user->id;
        $item->save();

        return response()->json($item, 200);
    }

    public function join (Request $request) {
        return response()->json(['outputs' => $outputs, 'code' => $code, 'base' => base_path()], 200);
    }

    public function speak (Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required',
            'start' => 'required',
            'end' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 400);
        }
        $call = Call::find($request->input('call_id'));
        if (!empty($call)) {
            $user = User::first(); // TODO: Replace with $request->user later

            $audio = $request->file('audio');

            $item = new Sentence();
            $item->id = (string) Str::orderedUuid();
            $item->call_id = $request->input('call_id');
            $item->user_id = $user->id;
            $item->recording = 'storage/audio/'.$call->id.'/'.$item->id.'.'.$audio->extension();
            $item->start = $request->input('start');
            $item->end = $request->input('end');
            $item->latitude = $request->input('latitude');
            $item->longitude = $request->input('longitude');

            if ($item->save()) {
                $audio->move('storage/audio/'.$call->id, $item->id.'.'.$audio->extension());

                $vnev = config('whisper.vnev');
                $model = config('whisper.model');
                $language = config('whisper.language');
                $model_dir = config('whisper.model_dir');
                $cache_dir = config('whisper.cache_dir');

                $outputs = [];
                $code = null;
                $command = $vnev.'/bin/python3 '.$vnev.'/bin/whisper '.$item->recording.' --model '.$model.' --language '.$language.' --model_dir='.$model_dir.' --output_format txt --output_dir '.$cache_dir;

                $success = exec($command, $outputs, $code);
                if (!empty($success)) {
                    $lines = [];
                    foreach ($outputs as $output) {
                        $parse = explode(']', $output);
                        if (count($parse) > 1) {
                            $lines[] = trim($parse[1]);
                        }
                    }
                    $item->text = implode(' ', $lines);
                    $item->whisper_output = json_encode($outputs);
                    $item->save();

                    $item->load('call');
                } else {
                    return response()->json(['message' => $code, 'command' => $command], 500);
                }
            }

            return response()->json($item, 200);
        } else {
            return response()->json(['message' => 'Invalid call id'], 400);
        }
    }

    public function stop (Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 400);
        }
        $item = Call::find($request->input('call_id'));
        if (!empty($item)) {
            $user = User::first(); // TODO: Replace with $request->user later

            $item->ended_at = (string) Carbon::now();
            $item->ended_by = $user->id;
            $item->save();

            return response()->json($item, 200);
        } else {
            return response()->json(['message' => 'Invalid call id'], 400);
        }
    }
}
