<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;
use App\Models\Sentence;

class WordCloudController extends Controller
{

    public function index(Request $request): Response
    {
        $sentences = Sentence::get();

        $counts = [];
        foreach ($sentences as $sentence) {
            $words = explode(' ', $sentence->text);
            foreach ($words as $word) {
                $word = strtolower($word);
                $word = trim($word);
                $word = str_replace(',', '', $word);
                $word = str_replace('.', '', $word);
                if (strlen($word) > 2) {
                    if (!isset($counts[$word])) {
                        $counts[$word] = 0;
                    }
                    $counts[$word]++;
                }
            }
        }

        $items = [];
        $highest = max($counts);
        foreach ($counts as $word => $count) {
            $items[] = [$word, ($count / $highest) * 100];
        }

        return Inertia::render('WordCloud', [
            'items' => $items
        ]);
    }
}
