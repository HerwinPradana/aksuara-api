<?php

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\ServiceProvider;

$base_path = base_path();

return [
    'vnev' => env('WHISPER_VNEV', $base_path.'/whisper-vnev'), // vnev path
    'model' => env('WHISPER_MODEL', 'small'), // tiny, base, small, medium, large
    'language' => env('WHISPER_LANGUAGE', 'Indonesian'),
    'model_dir' => env('WHISPER_MODEL_DIR', $base_path.'/whisper-models'),
    'cache_dir' => env('WHISPER_CACHE_DIR', $base_path.'/whisper-cache')
];
