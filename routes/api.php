<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\CallController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('call')->group(function () {
    Route::get('/map', [CallController::class, 'map']);
    Route::post('/start', [CallController::class, 'start']);
    Route::post('/join', [CallController::class, 'join']);
    Route::post('/speak', [CallController::class, 'speak']);
    Route::post('/stop', [CallController::class, 'stop']);
});

