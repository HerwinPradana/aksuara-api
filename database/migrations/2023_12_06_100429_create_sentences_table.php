<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sentences', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('call_id');
            $table->foreignUuid('user_id');
            $table->string('recording');
            $table->text('text')->nullable();
            $table->dateTimeTz('start');
            $table->dateTimeTz('end');
            $table->float('latitude', 10, 6);
            $table->float('longitude', 10, 6);
            $table->text('whisper_output')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->foreignUuid('created_by')->nullable();
            $table->foreignUuid('updated_by')->nullable();
            $table->foreignUuid('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sentences');
    }
};
