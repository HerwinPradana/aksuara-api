## About Aksuara

Aksuara is a voice-to-text (VTT) web app that provides both an API and a back-office for monitoring VTT data. Aksuara provides an easy-to-deploy solution for VTT using [Laravel 10](https://laravel.com/docs/10.x) and [OpenAI Whisper](https://github.com/openai/whisper). The back-office's UI is created using [Laravel's Breeze + Vue JS](https://laravel.com/docs/10.x/starter-kits#breeze-and-inertia). By default, Aksuara is configured to perform VTT in the Indonesian language, but this can be changed in the configuration file.
```
config\whisper.php
```

**Whisper will download the selected pre-trained model if it hasn't existed in the designated models folder. This means your first ever VTT request will take a long time due to the download.**

## Installation

1. Clone this repository
2. Enter the app's root folder
```
cd [app-folder]
```
2. Prepare OpenAI Whisper by making a python 3 virtualenv and other folders
```
mkdir whisper-models
mmkdir whisper-cache
virtualenv -p python3 whisper-vnev
```
3. Inside Whisper's virtual env, install Whisper
```
pip install -U openai-whisper
```
4. **Make sure the whisper-models and whisper-cache folders are writeable by your web server.**
5. Edit .env to connect to your MySQL database
6. Install Laravel
```
composer install
php artisan migrate
php artisan db:seed
npm install
php artisan storage:link
```

## Starting the App
1. Start Laravel
```
php artisan serve
```
2. Start the UI
```
npm start
```
Or for development mode
```
npm run dev
```

